#ifndef COLOR_H
# define COLOR_H

#include <stdlib.h>

#include "libparser.h"
#include "calcmath.h"

vec3_t *init_image(size_t size);

void set_color(struct scene *s, vec3_t *image, vec3_t color, size_t index);

#endif /* ! COLOR_H */
