#ifndef WRITE_H
# define WRITE_H

#include <stdio.h>

#include "libparser.h"
#include "calcmath.h"

void write_file(char *path, struct scene *v, vec3_t *pixel);

#endif /* ! WRITE_H */
