#ifndef RAYINIT_H
# define RAYINIT_H

#include "libparser.h"
#include "calcmath.h"
#include "color.h"

//file get_scene.c
vec3_t get_middle(struct camera cam);

vec3_t get_right(struct camera cam);

vec3_t get_B(struct scene *s, vec3_t pixel, vec3_t middle);

vec3_t get_origin(struct camera cam);


//file intersection.c
double intersection_plane(struct object obj, vec3_t *vtx, vec3_t origin, vec3_t ray);


//file rayinit.c
vec3_t rayinit(struct scene *s, vec3_t pixel, vec3_t P, vec3_t middle);

void Raytrace(struct scene *s, vec3_t *image);

#endif /* ! RAYINIT_H */
