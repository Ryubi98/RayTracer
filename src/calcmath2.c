#include "calcmath.h"

vec3_t normalize_vect(vec3_t a)
{
    double denom = sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
    vec3_t res = {
        .x = a.x / denom,
        .y = a.y / denom,
        .z = a.z / denom
    };
    return res;
}

double dist_twopts(vec3_t a, vec3_t b)
{
    double sum = pow(a.x - b.x, 2) + pow(a.y - b.y, 2) + pow(a.z - b.z, 2);
    return sqrt(sum);
}

vec3_t crtvect(vec3_t a, vec3_t b)
{
    vec3_t res = {
        .x = b.x - a.x,
        .y = b.y - a.y,
        .z = b.z - a.z
    };
    return res;
}
