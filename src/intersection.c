#include "rayinit.h"

double intersection_plane(struct object obj, vec3_t *vtx, vec3_t origin,
                          vec3_t ray)
{
    double epsilon = 0.0000001;

    vec3_t sum0 = add_vect(vtx[0], obj.position);
    vec3_t sum1 = add_vect(vtx[1], obj.position);
    vec3_t sum2 = add_vect(vtx[2], obj.position);

    vec3_t edge1 = sub_vect(sum1, sum0);
    vec3_t edge2 = sub_vect(sum2, sum0);

    vec3_t h = crossp_vect(ray, edge2);
    double a = scalar_vect(edge1, h);

    if (a > -epsilon && a < epsilon)
        return 0;

    double f = 1.0/a;

    vec3_t s = sub_vect(origin, sum0);
    double u = f * scalar_vect(s, h);

    if (u < 0.0 || u > 1.0)
        return 0;

    vec3_t q = crossp_vect(s, edge1);
    double v = f * scalar_vect(ray, q);
    if (v < 0.0 || u + v > 1.0)
        return 0;

    double t = f * scalar_vect(edge2, q);
    return t;
}
