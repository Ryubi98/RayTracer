#include "rayinit.h"

vec3_t get_middle(struct camera cam)
{
    vec3_t middle = add_vect(cam.position, cam.forward);
    return middle;
}

vec3_t get_right(struct camera cam)
{
    vec3_t right = crossp_vect(cam.forward, cam.up);
    return right;
}

vec3_t get_origin(struct camera cam)
{
    return cam.position;
}
