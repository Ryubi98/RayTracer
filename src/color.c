#include "color.h"

vec3_t *init_image(size_t size)
{
    vec3_t *image = malloc(sizeof(*image) * size);
    if (!image)
        return NULL;

    vec3_t black =
    {
        .x = 0,
        .y = 0,
        .z = 0
    };

    for (size_t i = 0; i < size; i++)
        image[i] = black;

    return image;
}

void set_color(struct scene *s, vec3_t *image, vec3_t color, size_t index)
{
    vec3_t new = { 0 };
    for (size_t i = 0; i < s->light_count; i++)
    {
        struct light color_light = s->lights[i];
        if (color_light.type == AMBIENT)
            new = add_vect(new, color_light.color);
        else if (color_light.type == POINT)
            continue;
        else // color == DIRECTIONNAL
            continue;
    }
    new.x *= color.x;
    new.y *= color.y;
    new.z *= color.z;
    image[index] = normalize_vect(new);
}
