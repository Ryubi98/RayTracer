#include "rayinit.h"

vec3_t rayinit(struct scene *s, vec3_t pixel, vec3_t P, vec3_t middle)
{
    struct camera cam = s->camera;
    vec3_t right = get_right(cam);
    right.x = -right.x;
    right.y = -right.y;
    right.z = -right.z;

    double h = s->height;
    double w = s->width;

    vec3_t scaleU = scale_vect((h/2.0 - pixel.y) / (h/2.0), cam.up);
    vec3_t scaleR = scale_vect((pixel.x - w/2.0) / (w/2.0), right);

    vec3_t B =
    {
        .x = middle.x + scaleU.x + scaleR.x,
        .y = middle.y + scaleU.y + scaleR.y,
        .z = middle.z + scaleU.z + scaleR.z
    };

    return sub_vect(P, B);
}

void Raytrace(struct scene *s, vec3_t *image)
{
    struct camera cam = s->camera;
    vec3_t middle = get_middle(cam);
    vec3_t P = get_origin(cam);

    for (double i = 0; i < s->width; i++)
    {
        for (double j = 0; j < s->height; j++)
        {
            vec3_t new_pixel = {
                .x = i,
                .y = j,
            };
            vec3_t ray = rayinit(s, new_pixel, P, middle);
            for (size_t k = 0; k < s->object_count; k++)
            {
                struct object obj = s->objects[k];
                struct mesh m = s->meshs[obj.mesh_id];
                size_t count = 3 * m.tri_count;
                for (size_t l = 0; l < count; l += 3)
                {
                    if (intersection_plane(obj, m.vtx + l, P, ray))
                    {
                        int i_int = i;
                        int j_int = j;
                        vec3_t color = s->mtls[obj.mtl_id].diffuse;
                        set_color(s, image, color, j_int * s->width + i_int);
                    }
                }
            }
        }
    }
}
