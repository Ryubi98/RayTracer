#ifndef CALCMATH_H
# define CALCMATH_H

#include <math.h>

#include "libparser.h"

//file calcmath.c
vec3_t add_vect(vec3_t a, vec3_t b);

vec3_t sub_vect(vec3_t a, vec3_t b);

vec3_t scale_vect(double scal, vec3_t a);

double scalar_vect(vec3_t a, vec3_t b);

vec3_t crossp_vect(vec3_t a, vec3_t b);


//file calcmath2.c
vec3_t normalize_vect(vec3_t a);

double dist_twopts(vec3_t a, vec3_t b);

vec3_t crtvect(vec3_t a, vec3_t b);

#endif /* ! CALCMATH_H */
