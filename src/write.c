#include "write.h"

void write_file(char *path, struct scene *s, vec3_t *pixel)
{
    FILE *file = fopen(path, "w");
    if (!file)
        return;

    fprintf(file, "P3\n%d %d\n%d\n", s->width, s->height, 255);

    for (int i = 0; i < s->height; i++)
    {
        for (int j = 0; j < s->width; j++)
        {
            vec3_t p = pixel[i * s->width + j];
            p = scale_vect(255, p);
            fprintf(file, "%0.0lf %0.0lf %0.0lf   ", p.x, p.y, p.z);
        }
        fprintf(file, "\n");
    }

    fclose(file);
}
