#include "calcmath.h"

vec3_t add_vect(vec3_t a, vec3_t b)
{
    vec3_t res = {
        .x = a.x + b.x,
        .y = a.y + b.y,
        .z = a.z + b.z
    };
    return res;
}

vec3_t sub_vect(vec3_t a, vec3_t b)
{
    vec3_t res = {
        .x = a.x - b.x,
        .y = a.y - b.y,
        .z = a.z - b.z
    };
    return res;
}

vec3_t scale_vect(double scal, vec3_t a)
{
    vec3_t res = {
        .x = scal * a.x,
        .y = scal * a.y,
        .z = scal * a.z
    };
    return res;
}

double scalar_vect(vec3_t a, vec3_t b)
{
    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}

vec3_t crossp_vect(vec3_t a, vec3_t b)
{
    vec3_t res = {
        .x = (a.y * b.z) - (a.z * b.y),
        .y = (a.z * b.x) - (a.x * b.z),
        .z = (a.x * b.y) - (a.y * b.x)
    };
    return res;
}
