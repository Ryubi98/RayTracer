CC ?= clang
override CFLAGS += -Wall -Wextra -std=c99 -pedantic -O3 \
		 -D_POSIX_C_SOURCE=200809L \
		 -I./parser

LDFLAGS = -L.
LDLIBS = -lm
VPATH = src

RT_NAME = rt
RT_OBJS =			\
	main.o 			\
	write.o			\
	calcmath.o		\
	calcmath2.o		\
	color.o			\
	get_scene.o		\
	intersection.o	\
	rayinit.o		\

LIBPARSER=libparser.a


all: $(RT_NAME)

$(RT_NAME): $(RT_OBJS) $(LIBPARSER)
	$(CC) $(RT_OBJS) $(LIBPARSER) $(LDFLAGS) $(LDLIBS) $(LIBS) -o $@

parser/$(LIBPARSER):
	make -C parser

$(LIBPARSER): parser/$(LIBPARSER)
	mv parser/$(LIBPARSER) $@

proper:
	$(RM) $(RT_OBJS)

clean: proper
	$(RM) $(RT_NAME) $(LIBPARSER) *.ppm
	make -C parser clean

.PHONY: all $(RT_NAME) $(LIBPARSER) parser/$(LIBPARSER) proper clean

